" html/spelllang.vim: Attempt to detect 'spelllang' from HTML buffers

" Don't load if running compatible or too old
if &compatible || v:version < 802
  finish
endif

" Don't load if already loaded
if exists('b:did_ftplugin_html_spelllang')
  finish
endif

" Flag as loaded
let b:did_ftplugin_html_spelllang = 1
let b:undo_ftplugin .= '|unlet b:did_ftplugin_html_spelllang'

" Set up hooks for divining 'spelllang' from lang= attributes
augroup html_spelllang
  if exists('##TextChanged')
    autocmd TextChanged *
          \ if &modified
          \|  call html#spelllang#Set()
          \|endif
  else
    autocmd InsertLeave *
          \ if &modified
          \|  call html#spelllang#Set()
          \|endif
  endif
augroup END
call html#spelllang#Set()
let b:undo_ftplugin .= '|execute ''autocmd! html_spelllang'''
      \ . '|augroup! html_spelllang'
