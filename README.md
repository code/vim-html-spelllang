html\_spelllang.vim
===================

This filetype plugin for HTML searches for `<html>`, `<body>`, and `<main>`
elements, and attempts to set the `'spelllang'` option appropriately if it
detects that spelling files exist corresponding to any `lang=` attribute
defined for the lattermost instances of these elements within the first 128
lines.

License
-------

Copyright (c) [Tom Ryder][1].  Distributed under the same terms as Vim itself.
See `:help license`.

[1]: https://sanctum.geek.nz/
